﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Net;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Newtonsoft.Json;
using RestSharp;
using Sharpy.Classes;

namespace Sharpy.Commands
{
    public class KeyOutput
    {
        public string key { get; set; }
    };
    class OwnerCommands: BaseCommandModule
    {
        [Command("eval"), Description("Compiles and runs C# code."), Aliases("compile", "exec", "e"), Hidden]
        public async Task EvalCommand(CommandContext ctx,
            [RemainingText, Description("The code to compile and execute.")]
            string code)
        {
            if (ctx.Message.Author.Id != 309427040908476416)
            {
                await Embeds.NotAnOwnerEmbed(ctx);
                return;
            }

            try
            {
                var state = await CSharpScript.RunAsync(Regex.Replace(code ?? "\"No output.\"", "```(cs|csharp)?", "").Trim(),
                    ScriptOptions.Default.WithReferences(typeof(int).Assembly,
                            typeof(CommandContext).Assembly,
                            typeof(Functions.EvalCtx).Assembly)
                        .WithImports("DSharpPlus", "DSharpPlus.Entities", "DSharpPlus.CommandsNext", "Sharpy", "System", "System.Linq"),
                    new Functions.EvalCtx(ctx), typeof(Functions.EvalCtx));
                if (state.Exception == null)
                {
                    DiscordEmbedBuilder emb = new DiscordEmbedBuilder()
                        .SetTitle("Eval")
                        .AddField("Input:", $"```cs\n{Regex.Replace(code ?? "No input.", "```(cs|csharp)?", "")}\n```")
                        .AddField("Output:", $"```cs\n{state.ReturnValue?.ToString() ?? "No output."}\n```")
                        .SetColor(DiscordColor.Green);
                    if (state.Variables.Any())
                        emb.AddField("Variables:",
                            "```cs\n" + string.Join("\n",
                                state.Variables.Select(variable =>
                                    $"{variable.Name} = {variable.Value} ({variable.Type})")) + "\n```");
                    await ctx.RespondAsync(embed: emb.Build());
                }
                else
                {
                    throw state.Exception;
                }
            }
            catch (Exception e)
            {
                if (e.ToString().Length > 1024)
                {
                    IRestClient restClient = new RestClient();
                    IRestRequest restRequest = new RestSharp.RestRequest()
                    {
                        Resource = "https://hastebin.com/documents"
                    };
                    restRequest.AddHeader("Content-Type", "application/json");
                    restRequest.AddJsonBody(e.ToString());
                    IRestResponse res = restClient.Post(restRequest);
                    KeyOutput key = JsonConvert.DeserializeObject<KeyOutput>(res.Content);
                    DiscordEmbedBuilder embe = new DiscordEmbedBuilder()
                        .SetTitle("Eval")
                        .AddField("Code:", $"```cs\n{code}\n```")
                        .AddField("Error:",
                            $"This field has exceeded the 1024 character limit. Uploaded the error to [hastebin](https://hastebin.com/raw/{key.key}).")
                        .SetColor(DiscordColor.Red);
                    await ctx.RespondAsync(embed: embe.Build());
                }
                else
                {
                    DiscordEmbedBuilder emb = new DiscordEmbedBuilder()
                        .SetTitle("Eval")
                        .AddField("Code:", $"```cs\n{code}\n```")
                        .AddField("Error:", $"```\n{e}\n```")
                        .SetColor(DiscordColor.Red);
                    await ctx.RespondAsync(embed: emb.Build());
                }
            }
        }
        [Command("sudo"), Description("Executes a command as another user."), Hidden, RequireOwner]
        public async Task Sudo(CommandContext ctx, [Description("Member to execute as.")] DiscordMember member, [RemainingText, Description("Command text to execute.")] string command)
        {
            var cmds = ctx.CommandsNext;
            var cmd = cmds.FindCommand(command, out var customArgs);
            var fakeContext = cmds.CreateFakeContext(member, ctx.Channel, command, ctx.Prefix, cmd, customArgs);
            await cmds.ExecuteCommandAsync(fakeContext);
        }

    }
}