﻿using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Newtonsoft.Json;
using RestSharp;

namespace Sharpy.Commands
{
    public class UrlOutput
    {
        public string url { get; }
    }
    public class ImageCommands : BaseCommandModule
    {
        [Command("badger"), Aliases("borsuk"), Description("Random badger image.")]
        public async Task BadgerCommand(CommandContext ctx)
        {
            IRestClient restClient = new RestClient();
            IRestRequest req = new RestRequest()
            {
                Resource = "https://multiapp.xyz/api/images/badgers"
            };
            req.AddHeader("token", Config.MultiappToken);
            IRestResponse res = restClient.Get(req);
            DiscordEmbedBuilder builder = new DiscordEmbedBuilder
            {
                Title = "Badger",
                ImageUrl = JsonConvert.DeserializeObject<UrlOutput>(res.Content).url,
                Footer =
                {
                    Text = "Powered by Multiapp.xyz",
                    IconUrl = "https://multiapp.xyz/favicon.ico"
                }
            };
            await ctx.RespondAsync(embed: builder.Build());
        }

        [Command("thanos"), Description("Random Thanos image.")]
        public async Task ThanosCommand(CommandContext ctx)
        {
            IRestClient restClient = new RestClient();
            IRestRequest req = new RestRequest()
            {
                Resource = "https://multiapp.xyz/api/images/thanos"
            };
            req.AddHeader("token", Config.MultiappToken);
            IRestResponse res = restClient.Get(req);
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder
            {
                Title = "Thanos",
                ImageUrl = JsonConvert.DeserializeObject<UrlOutput>(res.Content).url,
                Footer =
                {
                    Text = "Powered by Multiapp.xyz",
                    IconUrl = "https://multiapp.xyz/favicon.ico"
                }
            };
            await ctx.RespondAsync(embed: embed.Build());
        }
    }
}