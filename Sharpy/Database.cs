﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver;

namespace Sharpy
{
    public class Database : MongoClient
    {
        public Database() : base($"mongodb+srv://dbUser:{Config.DatabasePassword}@bot.zfeft.mongodb.net/Sharpy?retryWrites=true&w=majority")
        {
            
        }
    }
}
