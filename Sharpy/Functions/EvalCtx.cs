﻿using DSharpPlus.CommandsNext;

namespace Sharpy.Functions
{
    public class EvalCtx
    {
        public CommandContext ctx;

        public EvalCtx(CommandContext ctx)
        {
            this.ctx = ctx;
        }
    }
}