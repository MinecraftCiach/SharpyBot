﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using MongoDB.Driver;
using Sharpy.Classes;
using Sharpy.Models;

namespace Sharpy.Events
{
    public class MessageCreatedAsync
    {
        private static Database _database;
        public static async Task MessageCreated(DiscordClient client, MessageCreateEventArgs msg)
        {
            _database = new Database();
                var db = _database.GetDatabase("Sharpy");
                var col = db.GetCollection<GuildModel>("Config");
                var filter = Builders<GuildModel>.Filter.Eq("GuildId", msg.Guild.Id);
                var doc = await col.Find(filter).FirstOrDefaultAsync();
                if (msg.Message.Content == $"<@{client.CurrentUser.Id}>" ||
                    msg.Message.Content == $"<@!{client.CurrentUser.Id}>")
                {
                    DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                        .SetTitle("👋 Hello!")
                        .SetDescription($"I'm Sharpy! My prefix here is `{doc.Prefix}`. Nice to see you! ;)")
                        .SetColor(DiscordColor.Green);
                    await msg.Channel.SendMessageAsync(embed: embed.Build());
                    return;
                }
                var cnext = client.GetCommandsNext();
                var cmdStart = msg.Message.GetStringPrefixLength(doc.Prefix);
                if (cmdStart == -1) return;
                var prefix = msg.Message.Content.Substring(0, cmdStart);
                var cmdString = msg.Message.Content.Substring(cmdStart);
                var command = cnext.FindCommand(cmdString, out var cmdArgs);
                if (command == null)
                {
                    DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                            .SetTitle("Error")
                            .SetDescription($"`{cmdString}`: nonexistent command")
                            .SetColor(DiscordColor.Yellow)
                            .SetFooter($"Invoked by {msg.Author.Username}#{msg.Author.Discriminator}", msg.Author.AvatarUrl);
                    await msg.Channel.SendMessageAsync(embed: embed.Build());
                    return;
                }
                var ctx = cnext.CreateContext(msg.Message, prefix, command, cmdArgs);
                await cnext.ExecuteCommandAsync(ctx);
                return;
        }
    }
}