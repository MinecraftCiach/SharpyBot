﻿using System;
using System.Threading.Tasks;
using System.Timers;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using MongoDB.Driver;
using Sharpy.Models;
using Emzi0767.Utilities;

namespace Sharpy.Events
{
    public class ReadyAsync
    {
        private static Database _database;
        private static Timer _timer;
        public static async Task Ready(DiscordClient client, ReadyEventArgs evt)
        {
            try
            {
                _database = new Database();
                var db = _database.GetDatabase("Sharpy");
                var col = db.GetCollection<GuildModel>("Config");
                foreach (var guild in client.Guilds.Values)
                {
                    var filter = Builders<GuildModel>.Filter.Eq("GuildId", guild.Id);
                    try
                    {
                        col.Find(filter).FirstOrDefault();
                    }
                    catch (Exception e)
                    {
                        if (e is InvalidOperationException exc)
                        {
                            if (exc.Message == "Sequence contains no elements")
                            {
                                var g = guild;
                                var doc = new GuildModel
                                {
                                    GuildId = g.Id,
                                    Prefix = "s:"
                                };
                                await col.InsertOneAsync(doc);
                            }
                        }
                    }
                }

                var currentUser = client.CurrentUser;
                var tag = $"{currentUser.Username}#{currentUser.Discriminator}";
                Console.WriteLine($"{tag} is ready!");
                var random = new Random();
                string[] arr =
                {
                    "b!help",
                    $"{client.Guilds.Count} servers",
                    "PROTIP: DM __MinecraftCiach to get a special role on the support server. (Coming Soon)"
                };
                var game = new DiscordActivity(arr[random.Next(0, arr.Length)]);
                await client.UpdateStatusAsync(game);
                _timer = new Timer()
                {
                    Interval = 10000,
                    Enabled = true
                };

                async void ChangeStatus(object source, ElapsedEventArgs evtArgs)
                {
                    game.Name = arr[random.Next(0, arr.Length)];
                    await client.UpdateStatusAsync(game);
                }

                _timer.Elapsed += ChangeStatus;
            }
            catch (AsyncEventTimeoutException<DiscordClient, ReadyEventArgs> e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}