﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Text;
using Sharpy.Classes;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Interactivity.Enums;
using Sharpy.Commands;

namespace Sharpy
{
    public class Program
    {
        public static DiscordClient _client;
        public static async Task Main(string[] args) {
            _client = new DiscordClient(new DiscordConfiguration
            {
                Token = Config.Token,
                TokenType = TokenType.Bot,
                MinimumLogLevel = Microsoft.Extensions.Logging.LogLevel.Debug
            });
            CommandsNextExtension module = _client.UseCommandsNext(new CommandsNextConfiguration
            {
                UseDefaultCommandHandler = false
            });
            _client.UseInteractivity(new InteractivityConfiguration()
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(10)
            });

            _client.Ready += Events.ReadyAsync.Ready;
            _client.MessageCreated += Events.MessageCreatedAsync.MessageCreated;
            _client.GuildCreated += Events.GuildCreatedAsync.GuildCreated;
            module.CommandExecuted += Events.CommandsNext.CommandExecuted.CommandExecutedTask;
            module.CommandErrored += Events.CommandsNext.CommandErrored.CommandErroredTask;
            
            module.RegisterCommands<InfoCommands>();
            module.RegisterCommands<ModerationCommands>();
            module.RegisterCommands<OwnerCommands>();
            module.RegisterCommands<ConfigCommands>();
            module.RegisterCommands<ImageCommands>();
            await _client.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}
